# Changelog <!-- omit in toc -->

- [1.x.x](#1xx)
  - [1.1.0 - 24-Jan-2021](#110---24-jan-2021)
  - [1.0.5 - 12-Jan-2021](#105---12-jan-2021)
  - [1.0.4 - 02-Jan-2021](#104---02-jan-2021)
  - [1.0.3 - 28-Dec-2020](#103---28-dec-2020)
  - [1.0.2 - 28-Dec-2020](#102---28-dec-2020)
  - [1.0.1 - 28-Dec-2020](#101---28-dec-2020)
  - [1.0.0 - 28-Dec-2020](#100---28-dec-2020)
- [Pre-release versions](#pre-release-versions)
  - [0.1.1 - 28-Dec-2020](#011---28-dec-2020)
  - [0.1.0 - 28-Dec-2020](#010---28-dec-2020)

## 1.x.x

### 1.1.0 - 24-Jan-2021

- Replace build-script tests with macro tests
  - Should hopefully decrease build times
- Bumped `nom` to `6.1.0`

### 1.0.5 - 12-Jan-2021

- Bump `anyhow` to `1.0.38`

### 1.0.4 - 02-Jan-2021

- Bump `anyhow` to `1.0.37`

### 1.0.3 - 28-Dec-2020

- Fix [libraries.io](https://libraries.io/cargo/rfc2396) reference in `README.md`

### 1.0.2 - 28-Dec-2020

- Fix links in crate root's documentation

### 1.0.1 - 28-Dec-2020

- Fix illegal `package.keywords` in `Cargo.toml` (which was blocking publication)

### 1.0.0 - 28-Dec-2020

_Note: not published to https://crates.io; see version 1.0.1_

- Initial "stable" release and publication to [crates.io](https://crates.io/crates/rfc2396)

## Pre-release versions

### 0.1.1 - 28-Dec-2020

_Note: not published to https://crates.io_

- Fix CI version check
- Add auto-release CI job

### 0.1.0 - 28-Dec-2020

_Note: not published to https://crates.io_

- Initial pre-release version
