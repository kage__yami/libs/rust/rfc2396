pub fn recognize0<'a, F, E>(parser: F) -> impl FnMut(&'a str) -> nom::IResult<&'a str, &'a str, E>
where
    F: nom::Parser<&'a str, &'a str, E>,
    E: nom::error::ParseError<&'a str>,
{
    nom::combinator::recognize(nom::multi::many0_count(parser))
}

pub fn recognize1<'a, F, E>(parser: F) -> impl FnMut(&'a str) -> nom::IResult<&'a str, &'a str, E>
where
    F: nom::Parser<&'a str, &'a str, E>,
    E: nom::error::ParseError<&'a str>,
{
    nom::combinator::recognize(nom::multi::many1_count(parser))
}

pub fn recognize_tuple<'a, O, L, E>(list: L) -> impl FnMut(&'a str) -> nom::IResult<&'a str, &'a str, E>
where
    L: nom::sequence::Tuple<&'a str, O, E>,
    E: nom::error::ParseError<&'a str>,
{
    nom::combinator::recognize(nom::sequence::tuple(list))
}

pub fn recognize_pair<'a, O1, O2, F, G, E>(
    first: F,
    second: G,
) -> impl FnMut(&'a str) -> nom::IResult<&'a str, &'a str, E>
where
    F: nom::Parser<&'a str, O1, E>,
    G: nom::Parser<&'a str, O2, E>,
    E: nom::error::ParseError<&'a str>,
{
    nom::combinator::recognize(nom::sequence::pair(first, second))
}
