use anyhow::Context;

test_cases! {
    multi_char::query: {
        case_001: "!s!search%20terms",
        case_002: "!s-3!",
        case_003: "!s10!",
        case_004: "!s3!search%20terms",
        case_005: "!state",
        case_006: "%68%65%6c%6c%6f%20%77%6f%72%6c%64",
        case_007: "01h25m30s",
        case_008: "115Fragm8+-52f89c4c",
        case_009: ":words:some-context-for-a-(search-term)",
        case_010: ":~:text=foo",
        case_011: ":~:text=night-,vision",
        case_012: "bar",
        case_013: "broader",
        case_014: "cell=4,1-6,2",
        case_015: "col=2",
        case_016: "foo",
        case_017: "line=10,20",
        case_018: "match=%5brR%5d%5bfF%5d%5bcC%5d",
        case_019: "md5=38dc89f294b24691d3f0d893ed3c119c",
        case_020: "nameddest=",
        case_021: "page=35",
        case_022: "row=4",
        case_023: "row=5-*",
        case_024: "row=5-7",
        case_025: "search=%22word1%20word2%22",
        case_026: "t=10,20",
        case_027: "t=3m25s",
        case_028: "t=40,80&xywh=160,120,320,240",
        case_029: "view=fitb&nameddest=Chapter3",
        case_030: "xpath:/html/body/div%5b3%5d",
        case_031: "xpointer(//Rube)",
        case_032: "zoom="
    }
}
