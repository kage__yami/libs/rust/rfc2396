use anyhow::Context;

test_cases! {
    component::scheme: {
        case_data: "data",
        case_ftp: "ftp",
        case_http: "http",
        case_https: "https",
        case_irc: "irc",
        case_isbn: "isbn",
        case_mailto: "mailto",
        case_scheme: "scheme",
        case_steam: "steam",
        case_urn: "urn"
    }
}
