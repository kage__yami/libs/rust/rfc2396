use anyhow::Context;

test_cases! {
    component::abs_path: {
        case_001: "/!",
        case_002: "/!$",
        case_003: "/!Lqj",
        case_004: "/!uA5tE-",
        case_005: "/$%72OenW*j",
        case_006: "/$AZRf7n1",
        case_007: "/$n4v_",
        case_008: "/%02qT4p",
        case_009: "/%91Cp,&=A",
        case_010: "/&(IA7i)",
        case_011: "/'SS",
        case_012: "/'ZDd.n6/sf",
        case_013: "/(8DJ",
        case_014: "/(Wo1q2-+L",
        case_015: "/(t",
        case_016: "/*6",
        case_017: "/*8+",
        case_018: "/*wf-djL",
        case_019: "/+(*-3",
        case_020: "/-IzUr!OOP",
        case_021: "/-Qd",
        case_022: "/-vb@",
        case_023: "/-yS",
        case_024: "/01V9;!",
        case_025: "/04'0",
        case_026: "/18A9u1hFG",
        case_027: "/2460",
        case_028: "/2ad",
        case_029: "/3qm@*!Oya",
        case_030: "/4",
        case_031: "/6/2/n+Hkr8",
        case_032: "/6~d-m",
        case_033: "/7.HP$,4",
        case_034: "/7cvpFeU",
        case_035: "/;(sy5",
        case_036: "/;3",
        case_037: "/;Yfs_+",
        case_038: "/=Mqso~5K",
        case_039: "/=x",
        case_040: "/@7tW3Hx)iR",
        case_041: "/Bd",
        case_042: "/EE5",
        case_043: "/FNzKxq1",
        case_044: "/G",
        case_045: "/HIm0z~r",
        case_046: "/I",
        case_047: "/Jv*xi(Z",
        case_048: "/L89",
        case_049: "/LNz",
        case_050: "/Lj_ZwtSfWU",
        case_051: "/P!Q,m&4h",
        case_052: "/P/$3K",
        case_053: "/P:Z(MB/K.'",
        case_054: "/PMbLOz:q",
        case_055: "/PU:7W;1Hx",
        case_056: "/QKHg+d",
        case_057: "/R",
        case_058: "/SqvH,LcaAo",
        case_059: "/SuZUz=",
        case_060: "/T+RwC~",
        case_061: "/TQ+;Zq",
        case_062: "/V'g~RKeKq9",
        case_063: "/Yco%CDK7",
        case_064: "/ZHII2rYqWd",
        case_065: "/_",
        case_066: "/a)V,",
        case_067: "/aMrgo",
        case_068: "/bCr",
        case_069: "/bMLPeW.kt",
        case_070: "/d&MD4i",
        case_071: "/d480EFFd,",
        case_072: "/dn",
        case_073: "/eh",
        case_074: "/f:0!7Vu",
        case_075: "/fAMgR&cw",
        case_076: "/g",
        case_077: "/gkBO%C7aP2",
        case_078: "/h3",
        case_079: "/iG0",
        case_080: "/iz8(*Mj9d",
        case_081: "/j7DG!E+gc",
        case_082: "/jQf'",
        case_083: "/l%64@Q;DzZ",
        case_084: "/lAd",
        case_085: "/lkJk9M",
        case_086: "/m-;y",
        case_087: "/n.zgfdIJyh",
        case_088: "/nYpnz*&XNR",
        case_089: "/o",
        case_090: "/p1Q*(*",
        case_091: "/qI*O",
        case_092: "/rm91",
        case_093: "/ryDHGqKo",
        case_094: "/sQ@o;KME",
        case_095: "/u",
        case_096: "/wk",
        case_097: "/x",
        case_098: "/y+:QYUx$=",
        case_099: "/z5O'w",
        case_100: "/zy4&C"
    }
}
