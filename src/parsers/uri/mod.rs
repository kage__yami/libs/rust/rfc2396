/// `scheme ":" ( hier_part | opaque_part )`
pub fn absolute_uri(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_tuple((
        crate::parsers::component::scheme,
        nom::bytes::complete::tag(":"),
        nom::branch::alt((crate::parsers::component::hier_part, crate::parsers::component::opaque_part)),
    ))(i)
}

/// `( net_path | abs_path | rel_path ) [ "?" query ]`
pub fn relative_uri(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_pair(
        nom::branch::alt((
            crate::parsers::component::net_path,
            crate::parsers::component::abs_path,
            crate::parsers::component::rel_path,
        )),
        nom::combinator::opt(nom::sequence::pair(nom::bytes::complete::tag("?"), crate::parsers::multi_char::query)),
    )(i)
}

/// `[ absoluteURI | relativeURI ] [ "#" query ]`
pub fn uri_reference(i: &str) -> nom::IResult<&str, &str> {
    crate::combinators::recognize_pair(
        nom::combinator::opt(nom::branch::alt((crate::parsers::uri::absolute_uri, crate::parsers::uri::relative_uri))),
        nom::combinator::opt(nom::sequence::pair(nom::bytes::complete::tag("#"), crate::parsers::multi_char::query)),
    )(i)
}

#[cfg(test)]
mod tests;
