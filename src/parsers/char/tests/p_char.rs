use anyhow::Context;

test_cases! {
    char::p_char: {
        case_ampersand: "&",
        case_at: "@",
        case_colon: ":",
        case_comma: ",",
        case_dollar: "$",
        case_equals: "=",
        case_plus: "+"
    }
}
